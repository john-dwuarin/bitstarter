
var fs = require('fs');
var buffer = require('buffer');
var buf = fs.readFileSync("index.html");
var fileContent = buf.toString('utf8');

var express = require('express');

var app = express.createServer(express.logger());

app.get('/', function(request, response) {
  response.send(fileContent);
});

var port = process.env.PORT || 5000;
app.listen(port, function() {
  console.log("Listening on " + port);
});
